import numpy as np
import torch
import glob
import os
import re

from torch.utils.data import Dataset
from PIL import Image
from torchvision import transforms

class LineDataset(Dataset):
  """Line image dataset"""

  def __init__(self, img_dir, nbins, symm=True):
    """
    Args:
        img_dir : directory containing the .jpg files
        nbins   : no. of "distinct" line groups
        symm    : flag for symmetric bins
    """
    files = glob.glob(os.path.join(img_dir, '*.jpg'))
    n = len(files)
    
    # Hardcoded params
    new_size = (50, 50)
    pat = 'sG(\d*).jpg'
    img2ten = transforms.ToTensor()

    if symm:
      # Binsize should be over 90 to account for symmetry wrt vertical
      binsize = 90 // nbins
    else:
      binsize = 180 // nbins
    images = []
    labels = []

    for f in files:
      fname = os.path.basename(f)
      angle = int(re.match(pat, fname)[1])
      if (angle > 180):
        angle = angle - 180

      img = Image.open(f)
      img = img.rotate(90)
      img.thumbnail(new_size)
      ten = img2ten(img)

      if symm:
        if (angle > 90):
          theta = 180 - angle
        else:
          theta = angle

        lab = theta // binsize
      else:
        lab = angle // binsize

      if (lab >= nbins):
        lab = nbins - 1

      images.append(ten)
      labels.append(lab)
      
    # Convert to torch arrays
    self.lab = torch.tensor(labels)
    self.X   = torch.stack(images)

  def __len__(self):
    return len(self.X)

  def __getitem__(self, idx):
    if torch.is_tensor(idx):
      idx = idx.tolist()

    return self.X[idx,:], self.lab[idx]

