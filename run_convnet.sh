#!/bin/sh
#############################################################################
# TODO: Modify the hyperparameters such as hidden layer dimensionality, 
#       number of epochs, weigh decay factor, momentum, batch size, learning 
#       rate mentioned here to achieve good performance
#############################################################################
##for h in 10 13 16 19 22 25
#for h in 1 2 3 4 5 6 7 8 9
#  do
#    python -u train.py \
#    --model convnet \
#    --kernel-size 3 \
#    --hidden-dim $((h)) \
#    --epochs 500 \
#    --weight-decay 0.1 \
#    --momentum 0.9 \
#    --batch-size 90 \
#    --lr 0.0001 | tee logs/convnet_hd$h.log
#  done

# Try 3 bins (1-10 hidden size) and < 10 hidden dim size
#for h in 1 2 3 4 5 6 7 8 9 10
#  do
#    python -u train.py \
#    --model convnet \
#    --kernel-size 3 \
#    --hidden-dim $((h)) \
#    --epochs 500 \
#    --weight-decay 0.1 \
#    --momentum 0.9 \
#    --batch-size 90 \
#    --lr 0.0001 | tee logs/convnet_b3_hd$h.log
#  done

for h in 1
  do
    python -u train.py \
    --model convnet \
    --kernel-size 3 \
    --hidden-dim $((h)) \
    --epochs 500 \
    --weight-decay 0.1 \
    --momentum 0.9 \
    --batch-size 90 \
    --lr 0.001 | tee logs/convnet_optimal.log
  done
#############################################################################
#                             END OF YOUR CODE                              #
#############################################################################
