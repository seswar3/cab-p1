import numpy as np
import matplotlib.pyplot as plt


hd = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 16, 19, 22, 24]
params = [16838, 33666, 50494, 67322, 84150, 100978, 117806, 134634, 151462, 168290, 218774, 269258, 319742, 370226, 403882]
accuracy = [0.89, 0.92, 0.89, 0.92, 0.92, 0.86, 0.89, 0.95, 0.89, 0.97, 0.95, 0.89, 0.89, 0.92, 1.00]


plt.figure(0)
plt.scatter(hd, params)
plt.plot(hd, params, '-m', linewidth=1, label='# Parameters')
plt.legend(loc = 'best')

plt.title("Parameters vs Hidden Dimension", fontsize=20)
plt.xlabel('Number of Filters (Hidden Dimension)', fontsize=18)
plt.ylabel('Number of Parameters', fontsize=16)
plt.savefig("Parameters_Graph", bbox_inches='tight')

plt.figure(1)
plt.scatter(hd, accuracy)
plt.plot(hd, accuracy, '-c', linewidth=1, label='Accuracy')
plt.legend(loc = 'best')

plt.title("Accuracy vs Hidden Dimension", fontsize=20)
plt.xlabel('Number of Filters (Hidden Dimension)', fontsize=18)
plt.ylabel('Accuracy', fontsize=16)
plt.savefig("Accuracy_Graph", bbox_inches='tight')