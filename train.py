# NOTE: The scaffolding code for this part of the assignment
# is adapted from https://github.com/pytorch/examples.
from __future__ import print_function
import argparse
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import transforms
from torch.autograd import Variable

# You should implement these (softmax.py, twolayernn.py, convnet.py)
import models.convnet
from linedata import LineDataset

# Training settings
parser = argparse.ArgumentParser(description='CIFAR-10 Example')
# Hyperparameters
parser.add_argument('--lr', type=float, metavar='LR',
                    help='learning rate')
parser.add_argument('--momentum', type=float, metavar='M',
                    help='SGD momentum')
parser.add_argument('--weight-decay', type=float, default=0.0,
                    help='Weight decay hyperparameter')
parser.add_argument('--batch-size', type=int, metavar='N',
                    help='input batch size for training')
parser.add_argument('--epochs', type=int, metavar='N',
                    help='number of epochs to train')
parser.add_argument('--model',
                    choices=['softmax', 'convnet', 'twolayernn', 'mymodel'],
                    help='which model to train/evaluate')
parser.add_argument('--hidden-dim', type=int,
                    help='number of hidden features/activations')
parser.add_argument('--kernel-size', type=int,
                    help='size of convolution kernels/filters')
# Other configuration
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                    help='input batch size for testing (default: 1000)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='number of batches between logging train status')
parser.add_argument('--cifar10-dir', default='data',
                    help='directory that contains cifar-10-batches-py/ '
                         '(downloaded automatically if necessary)')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

# Load dataset
kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
n_classes = 10
# n_classes = 3
train_pct  = 0.8
val_pct    = 0.1
split_seed = 17
# split_gen  = torch.Generator().manual_seed(split_seed)
torch.manual_seed(split_seed)

line_dataset = LineDataset('data', n_classes)
im_size = tuple(line_dataset[0][0].shape)

train_size = int(train_pct * len(line_dataset))
val_size   = int(val_pct * len(line_dataset))
test_size  = len(line_dataset) - train_size - val_size

# train_dataset, val_dataset, test_dataset = torch.utils.data.random_split(line_dataset,
#                                             [train_size, val_size, test_size],
#                                             generator=split_gen)
train_dataset, val_dataset, test_dataset = torch.utils.data.random_split(line_dataset,
                                            [train_size, val_size, test_size])

# DataLoaders
train_loader = torch.utils.data.DataLoader(train_dataset,
                 batch_size=args.batch_size, shuffle=True, **kwargs)
val_loader = torch.utils.data.DataLoader(val_dataset,
                 batch_size=args.batch_size, shuffle=True, **kwargs)
test_loader = torch.utils.data.DataLoader(test_dataset,
                 batch_size=args.batch_size, shuffle=True, **kwargs)

# Load the model (only convnet)
model = models.convnet.CNN(im_size, args.hidden_dim, args.kernel_size,
                               n_classes)
# cross-entropy loss function
criterion = F.cross_entropy
if args.cuda:
    model.cuda()

#############################################################################
# TODO: Initialize an optimizer from the torch.optim package using the
# appropriate hyperparameters found in args. This only requires one line.
#############################################################################
if args.model == 'mymodel':
  #optimizer = torch.optim.RMSprop(model.parameters(), lr=args.lr, momentum=args.momentum, weight_decay=args.weight_decay);
  #optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay);
  optimizer = torch.optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum, weight_decay=args.weight_decay);
else:
  optimizer = torch.optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum, weight_decay=args.weight_decay);
#############################################################################
#                             END OF YOUR CODE                              #
#############################################################################

def train(epoch):
    '''
    Train the model for one epoch.
    '''
    # Some models use slightly different forward passes and train and test
    # time (e.g., any model with Dropout). This puts the model in train mode
    # (as opposed to eval mode) so it knows which one to use.
    best_acc = 0.0;
    model.train()
    # train loop
    for batch_idx, batch in enumerate(train_loader):
        # prepare data
        images, targets = Variable(batch[0]), Variable(batch[1])
        if args.cuda:
            images, targets = images.cuda(), targets.cuda()
        #############################################################################
        # TODO: Update the parameters in model using the optimizer from above.
        # This only requires a couple lines of code.
        #############################################################################
        optimizer.zero_grad();
        y_pred = model(images);
        loss = criterion(y_pred, targets);
        loss.backward();
        optimizer.step();
        
        #############################################################################
        #                             END OF YOUR CODE                              #
        #############################################################################
        if batch_idx % args.log_interval == 0:
            val_loss, val_acc = evaluate('val', n_batches=4)
            if (val_acc > best_acc) :
              best_acc = val_acc
              torch.save(model, 'trained_models/' + args.model + "_optimal_" + '_best.pt')
            train_loss = loss.data
            examples_this_epoch = batch_idx * len(images)
            epoch_progress = 100. * batch_idx / len(train_loader)
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\t'
                  'Train Loss: {:.6f}\tVal Loss: {:.6f}\tVal Acc: {}'.format(
                epoch, examples_this_epoch, len(train_loader.dataset),
                epoch_progress, train_loss, val_loss, val_acc))

def evaluate(split, verbose=False, n_batches=None):
    '''
    Compute loss on val or test data.
    '''
    model.eval()
    loss = 0
    correct = 0
    n_examples = 0
    if split == 'val':
        loader = val_loader
    elif split == 'test':
        loader = test_loader
    for batch_i, batch in enumerate(loader):
        data, target = batch
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = model(data)
        loss += criterion(output, target, size_average=False).data
        # predict the argmax of the log-probabilities
        pred = output.data.max(1, keepdim=True)[1]
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()
        n_examples += pred.size(0)
        if n_batches and (batch_i >= n_batches):
            break

    loss /= n_examples
    acc = 100. * correct / n_examples
    if verbose:
        print('\n{} set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
            split, loss, correct, n_examples, acc))
    return loss, acc


# train the model one epoch at a time
for epoch in range(1, args.epochs + 1):
    train(epoch)
    if args.model == 'mymodel':
      if epoch % 30 == 0:
        for g in optimizer.param_groups:
          g['lr'] = g['lr'] / 10.0;

evaluate('test', verbose=True)

# Save the model (architecture and weights)
torch.save(model, 'trained_models/' + args.model + "_optimal_" + '.pt')
# Later you can call torch.load(file) to re-load the trained model into python
# See http://pytorch.org/docs/master/notes/serialization.html for more details

print("Parameter Analysis")
total_param_num = 0

for n, p in model.named_parameters():
    print("\nParameter Name: ", n)
    print("Parameter Data Shape: ", p.data.shape)
    a = p.data.shape.numel()
    print("Number of Params in Layer: ", a)
    total_param_num += a

print("\nTotal Number of Params: ", total_param_num)


