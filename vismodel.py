import numpy as np
import matplotlib.pyplot as plt
import torch
import glob
import os
import re

from PIL import Image
from torchvision import transforms

# can make this inputs if script is needed
img_dir    = 'data'
model_name = 'trained_models/convnet_optimal_.pt'
inp_angle  =  60

# Load model (CPU)
model = torch.load(model_name, map_location=torch.device('cpu'))
# Load model (GPU) - uncomment if needed
# model = torch.load(model_name)

def get_img(angle):
  fname = os.path.join(img_dir, 'sG{}.jpg'.format(angle))

  new_size = (50, 50)
  img2ten  = transforms.ToTensor()

  # Read and transform the image
  img = Image.open(fname)
  img = img.rotate(90)
  img.thumbnail(new_size)
  ten = img2ten(img)

  return img, ten

# Activations registration
activation = {}
def get_activation(name):
  def hook(model, input, output):
    activation[name] = output.detach()
  return hook

model.conv1.register_forward_hook(get_activation('conv1'))

ten2img = transforms.ToPILImage()

img, x = get_img(inp_angle)
data = x.unsqueeze(0)
output = model(data)

print(activation['conv1'])
print(activation['conv1'].shape)

act = activation['conv1'].squeeze()

# Following only work for num_filters = 1
out_dir = 'figures'

if (len(act.shape) > 2):
  num_filters = act.shape[0]

  # Loop over all filters
  for f in range(act.shape[0]):
    plt.figure()
    plt.imshow(act[f])
    plt.savefig(os.path.join(out_dir, 'act_{}.pdf'.format(f)))
else:
  plt.imshow(act)
  plt.savefig(os.path.join(out_dir, 'act_0.pdf'))

for n, p in model.named_parameters():
  if (n == 'fc1.weight'):
    W = p.data

for i in range(W.shape[0]):
  plt.imshow(W[i,:].reshape(act.shape[0], act.shape[1]))
  plt.savefig(os.path.join(out_dir, 'W_{}.pdf'.format(i)))

# plot all in same plot
plt.figure()
f, axarr = plt.subplots(2, 5)
k = 0
for i in range(2):
  for j in range(5):
    axarr[i,j].imshow(W[k,:].reshape(act.shape[0], act.shape[1]))
    k = k + 1

plt.savefig(os.path.join(out_dir, 'W_all.pdf'.format(i)))
